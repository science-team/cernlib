#!/bin/sh -e

# install-cernlib-dirs.sh: Script to create a skeleton CERNLIB directory
# structure populated by symlinks to actual file locations.  This script is
# documented fully in the README.Debian file for the cernlib-base package.

# Copyright (C) 2006, 2007 Kevin B. McCarty <kmccarty@debian.org>.  This script
# is licensed under the GNU General Public Licence, version 2 or any later
# version (at your option).

msg_inform() {
	echo ; echo " I: $1"
}

msg_warn()   {
	echo ; echo " W: $1" 1>&2
}

msg_error()  {
	echo ; echo " E: $1  Aborting." 1>&2 ; exit 1
}

create_dir() {
	echo -n "Attempting to create directory $1 ... "
	if [ -d "$1" ] ; then
		if [ -L "$1" ] ; then
			msg_warn "Following existing symlink $1."
		else
			msg_inform "Using existing directory."
		fi
	elif [ -e "$1" ] ; then
		msg_error "$1 already exists as a non-directory!"
	elif [ -L "$1" ] ; then
		msg_warn "Replacing broken symlink $1 with directory."
		rm "$1" || msg_error "Unable to delete broken symlink $1!"
		mkdir -p "$1" || msg_error "Unable to create directory $1!"
	else
		mkdir -p "$1" || msg_error "Unable to create directory $1!"
		echo "done."
	fi
}

create_link() {
	echo -n "Attempting to create symlink $2 -> $1 ... "
	if [ -e "$2" ] && [ ! -L "$2" ] ; then
		msg_error "Will not replace existing $2 with symlink!"
	elif [ -L "$2" ] ; then
		target="`ls -ld "$2" | sed 's/^.* -> //'`"
		if [ "$target" = "$1" ] ; then
			msg_inform "Desired symlink already exists."
		else
			msg_warn \
			"Will not replace existing symlink $2 -> $target"
			msg_warn "with new symlink $2 -> $1."
		fi
	else
		ln -s "$1" "$2" || \
			msg_error "Unable to create symlink $2 -> $1!"
		echo "done."
	fi
}

destroy_dir() {
	if [ ! -e "$1" ] && [ ! -L "$1" ] ; then
		echo "No need to remove nonexistent $1"
		return
	fi
	
	echo -n "Attempting to remove directory $1 ... "
	if [ -d "$1" ] ; then
		if [ -L "$1" ] ; then
			msg_warn "$1 is a symlink; not touching."
		elif rmdir "$1" > /dev/null 2>&1 ; then
			echo "done."
		elif rmdir --ignore-fail-on-non-empty "$1" ; then
			msg_warn "$1 is not empty; not touching."
		else
			msg_warn "Unable to remove directory $1."
		fi
	elif [ -L "$1" ] && [ ! -e "$1" ] ; then
		msg_inform "It is a dead symlink; deleting it."
		rm "$1" || msg_error "Unable to delete symlink $1!"
	else
		msg_warn "$1 is not a directory; not touching."
	fi
}

destroy_link() {
	if [ ! -e "$1" ] && [ ! -L "$1" ] ; then
		echo "No need to remove nonexistent $1"
		return
	fi
	
	echo -n "Attempting to remove symlink $1 ... "
	if [ ! -L "$1" ] ; then
		msg_warn "$1 is not a symlink; not touching."
	else
		rm "$1" || msg_error "Unable to delete symlink $1!"
		echo "done."
	fi
}

# Defaults
yr=2006
uninstall=no
basedir=

while [ $# -gt 0 ] ; do
	case "$1" in
		--uninstall)	uninstall=yes ;;
		--install)	uninstall=no ;;
		--year)		shift ; yr="$1" ;;
		*)		basedir="$1" ;;
	esac
	shift
done

lvl="${yr}deb"	# Debian-specific value of $CERN_LEVEL we use
pfx="cern/$lvl"

if [ "$uninstall" = yes ] ; then
	destroy_link "$basedir/$pfx/src/config"
	destroy_link "$basedir/$pfx/src/include"
	destroy_link "$basedir/$pfx/src/pawlib/paw/ntuple"
	for dir in src/pawlib/paw src/pawlib src ; do
		destroy_dir "$basedir/$pfx/$dir"
	done	

	for dir in cspack epio fatmen ffread hbook hepdb kapack kuip minuit \
		zbook zebra kernbit kerngen kernnum comis paw sigma dzdoc \
		hplot higz gen geant321 cojets eurodec herwig59 isajet758 \
		jetset74 pdf804 pythia6205 cfortran stdhep ; do
		destroy_link "$basedir/$pfx/include/$dir"
	done
	destroy_dir "$basedir/$pfx/include"

	for datafile in xsneut95.dat cojets.dat isajet.dat eurodec.dat ; do
		destroy_link /usr/lib/$datafile
	done
	
	destroy_link "$basedir/$pfx/lib"
	destroy_link "$basedir/$pfx/bin"
	destroy_dir "$basedir/$pfx"
	destroy_dir "$basedir/cern/$yr"
	destroy_dir "$basedir/cern/dev"
	destroy_dir "$basedir/cern/pro"
	destroy_dir "$basedir/cern/old"
	destroy_dir "$basedir/cern"
	exit 0
fi

if [ -n "$basedir" ] && [ ! -d "$basedir" ] ; then
	create_dir "$basedir"
fi

create_dir "$basedir/cern"
create_dir "$basedir/$pfx"
create_link "$lvl" "$basedir/cern/$yr"
create_link "$lvl" "$basedir/cern/dev"
create_link "$lvl" "$basedir/cern/pro"
create_link "$lvl" "$basedir/cern/old"
create_link /usr/bin "$basedir/$pfx/bin"
create_link /usr/lib "$basedir/$pfx/lib"

for datafile in geant321-data/xsneut95.dat montecarlo-data/eurodec.dat \
		montecarlo-data/cojets.dat montecarlo-data/isajet.dat ; do
	create_link /usr/share/$datafile /usr/lib/$(basename $datafile)
done

create_dir "$basedir/$pfx/include"
for dir in cspack epio fatmen ffread hbook hepdb kapack kuip minuit zbook \
		zebra kernbit kerngen kernnum comis paw sigma dzdoc hplot \
		higz gen geant321 cojets eurodec herwig59 isajet758 jetset74 \
		pdf804 pythia6205 cfortran stdhep ; do
	create_link /usr/include/$dir "$basedir/$pfx/include/$dir"
done

create_dir "$basedir/$pfx/src"
create_link /usr/share/cernlib/config "$basedir/$pfx/src/config"
create_link "$basedir/$pfx/include" "$basedir/$pfx/src/include"

# At least as of 2004, ATLAS was trying to use the directory
# $CERN/$CERN_LEVEL/src/pawlib/paw/ntuple, which is installed at
# /usr/include/paw/ntuple to fix bug #243860
create_dir "$basedir/$pfx/src/pawlib/paw"
create_link /usr/include/paw/ntuple "$basedir/$pfx/src/pawlib/paw/ntuple"

