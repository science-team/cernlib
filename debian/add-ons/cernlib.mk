# Variables for installation directories, conforming to GNU standards
DEB_HOST_MULTIARCH ?= $(shell dpkg-architecture -qDEB_HOST_MULTIARCH)
prefix		= /usr/local
exec_prefix	= $(prefix)
bindir		= $(exec_prefix)/bin
sbindir		= $(exec_prefix)/sbin
datadir		= $(prefix)/share
sysconfdir	= $(prefix)/etc
libdir		= $(exec_prefix)/lib/$(DEB_HOST_MULTIARCH)
includedir	= $(prefix)/include
mandir		= $(prefix)/man
man1dir		= $(mandir)/man1
man8dir		= $(mandir)/man8
srcdir		= .
abssrcdir	= $(strip $(shell cd $(srcdir) && pwd))

# Other variables important to CERNLIB compilation
export ARCH	      = $(strip $(shell uname -s))
export CERN	      = $(shell pwd)
export CERN_LEVEL     = $(strip $(shell \
			tar tzf $(abssrcdir)/upstream/src_Imakefile.tar.gz \
			| cut -d / -f 1))
export CERN_ROOT      = $(CERN)
export CVSCOSRC	      = $(CERN_ROOT)/src
export CERN_DOCDIR    = $(CERN_ROOT)/doc
export CERN_BINDIR    = $(CERN_ROOT)/bin
export CERN_LIBDIR    = $(CERN_ROOT)/lib
export CERN_SHLIBDIR  = $(CERN_ROOT)/shlib
export CERN_BUILDDIR  = $(CERN_ROOT)/build

export SHELL	      = /bin/sh
export PATCHDIR	      = $(abssrcdir)/debian/patches
export ADDONDIR       = $(abssrcdir)/debian/add-ons
export LIB_SONAME     = 1
ifeq (,$(findstring ifort,$(DEB_BUILD_OPTIONS)))
	export TOOL_SONAME = _gfortran
	export TOOL_NAME   = _gfortran
endif
export PATH	   := $(CERN_BINDIR):$(ADDONDIR)/bin:$(datadir)/cernlib:${PATH}
export INSTALL_OPTS   ?= -s
export CC	      ?= gcc
export DEB_BUILD_ARCH ?= $(shell $(CC) -dumpmachine | \
				sed -e 's/-linux-gnu$$//' -e 's/-linux$$//')

# make sure patches are applied in the right order:
export LANG	      = C
export LC_COLLATE     = C

all: cernlib-indep cernlib-arch cernlib-test

cernlib-arch: stampdir/cernlib-arch-stamp
cernlib-indep: stampdir/cernlib-indep-stamp
cernlib-test: stampdir/cernlib-test-pass
gunzip: stampdir/gunzip-stamp
patch: stampdir/patch-stamp
install: install-indep install-arch

stampdir/cernlib-arch-stamp:
	[ -e stampdir/cernlib-arch-stamp ] || \
		$(MAKE) cernlib-libraries cernlib-programs cernlib-arch-local
	mkdir -p stampdir
	touch stampdir/cernlib-arch-stamp

cernlib-arch-local:

stampdir/cernlib-indep-stamp:
	[ -e stampdir/cernlib-indep-stamp ] || \
		$(MAKE) cernlib-indep-local
	mkdir -p stampdir
	touch stampdir/cernlib-indep-stamp

cernlib-indep-local:

stampdir/gunzip-stamp:
	if [ -d $(ADDONDIR)/bin ] ; then chmod a+x $(ADDONDIR)/bin/* ; fi
	for x in $(abssrcdir)/upstream/src_*.tar.gz ; do tar xzf $$x ; done
	# get rid of the needless extra directory whose name changes yearly
	mv $(CERN)/$(CERN_LEVEL)/src $(CERN)/
	rmdir $(CERN)/$(CERN_LEVEL)
	# get rid of the CVS directories
	find $(CVSCOSRC)/ -name CVS | xargs rm -rf

	# symlink in config directory if not already there
	set -e ; \
	if [ ! -d "$(CVSCOSRC)/config" ] ; then \
		if [ ! -d "$(datadir)/cernlib/config" ] ; then \
			echo "Cannot find required $(datadir)/cernlib/config!";\
			exit 1 ; \
		fi ; \
		mkdir $(CVSCOSRC)/config ; \
		for file in $(datadir)/cernlib/config/* ; do \
			ln -s $$file $(CVSCOSRC)/config/ ; \
		done ; \
	fi

	mkdir -p stampdir
	touch stampdir/gunzip-stamp

stampdir/configure-stamp: stampdir/gunzip-stamp
	# Add various patches to the patch directory based on environment
	# variable options
	for patch in `ls -1 $(PATCHDIR)/*.dpatch.in 2> /dev/null`; do \
		sed -e 's,@PREFIX@,$(prefix),g' -e 's,@DATADIR@,$(datadir),g' \
			-e 's,@MULTIARCH@,$(DEB_HOST_MULTIARCH),g' \
			$$patch > $${patch%.in} ; \
		chmod a+x $${patch%.in} ; \
	done

	# Set other options in src/config/host.def
	rm -f $(CVSCOSRC)/config/host.def
	echo '#define CernlibLocalDefines $(LOCAL_DEFINES)' >> \
		$(CVSCOSRC)/config/host.def
ifneq (,$(findstring noopt,$(DEB_BUILD_OPTIONS)))
	echo '#define OptimizationLevel -O0' >> $(CVSCOSRC)/config/host.def
endif
ifneq (,$(findstring nostrip,$(DEB_BUILD_OPTIONS)))
	echo '#define InstPgmFlags ' >> $(CVSCOSRC)/config/host.def
else
	echo '#define InstPgmFlags -s' >> $(CVSCOSRC)/config/host.def
endif
ifneq (,$(findstring ifort,$(DEB_BUILD_OPTIONS)))
	echo '#define HasIntelifc YES' >> $(CVSCOSRC)/config/host.def
else
	echo '#define Hasgfortran YES' >> $(CVSCOSRC)/config/host.def
endif
	$(MAKE) cernlib-configure-local
	mkdir -p stampdir
	touch stampdir/configure-stamp

cernlib-configure-local:

stampdir/patch-stamp: stampdir/configure-stamp
	# A hacky way to use dpatch without actually necessarily
	# having it installed.  Patches whose name ends in .sh.dpatch
	# are really shell scripts.
	@set -e ; \
	export LC_COLLATE=C ; \
	for patch in `ls -1 $(PATCHDIR)/*.dpatch 2> /dev/null \
		| grep -v 000-unpack.sh`; do \
		if [ "$$patch" = "$${patch%.sh.dpatch}" ] ; then \
			echo "Applying patch `basename $${patch%.dpatch}`..." ;\
			patch -f -p1 < "$$patch" > /dev/null ; \
		else \
			echo "Running script `basename $${patch%.dpatch}`..." ;\
			chmod a+x "$$patch" ; \
			"$$patch" -patch ; \
		fi ; \
	done
	mkdir -p stampdir
	touch stampdir/patch-stamp

buildtree: stampdir/patch-stamp
	mkdir -p $(CERN_BUILDDIR)/log
	cd $(CERN_BUILDDIR) && $(CVSCOSRC)/config/imake_boot
	set -e ; \
	for package in $(NEEDS_MAKEFILE) ; do \
		if [ -d $(CVSCOSRC)/$$package ] ; then \
			cd $(CERN_BUILDDIR)/ && $(MAKE) MAKEOVERRIDES= \
				$$package/Makefile ; \
		fi ; \
	done
	$(MAKE) cernlib-setup-local

cernlib-setup-local:

# build libraries (currently I can get shared libraries to compile only
# on Linux)
cernlib-libraries: buildtree
	set -e ; \
	for package in $(LIB_PACKAGES) ; do \
		if [ -d $(CVSCOSRC)/$$package ] ; then \
			cd $(CERN_BUILDDIR)/$$package && $(MAKE) \
				MAKEOVERRIDES= install.lib ; \
			if [ "$(ARCH)" = Linux ] ; then \
				cd $(CERN_BUILDDIR)/$$package && $(MAKE) \
					MAKEOVERRIDES= install.shlib ; \
			fi ; \
		fi ; \
	done ; \
	for file in $(CERN_LIBDIR)/lib*-shift.a ; do \
		if [ -L $$file ] ; then rm -f $$file ; fi ; \
	done

# build programs
cernlib-programs:
	set -e ; \
	for package in $(BIN_PACKAGES) ; do \
		if [ -d $(CVSCOSRC)/$$package ] ; then \
			cd $(CERN_BUILDDIR)/$$package && $(MAKE) \
				MAKEOVERRIDES= CERN_LEVEL=. install.bin ; \
		fi ; \
	done

# Test suite.  Run all tests even if some fail before the end.
stampdir/cernlib-test-pass: stampdir/cernlib-arch-stamp
ifeq (,$(findstring nocheck,$(DEB_BUILD_OPTIONS)))
	set -e ; \
	if [ "$(ARCH)" = Linux ] ; then \
		if [ -z "$$LD_LIBRARY_PATH" ] ; then \
			export LD_LIBRARY_PATH="$(CERN_SHLIBDIR)" ; \
		else \
			export LD_LIBRARY_PATH="$(CERN_SHLIBDIR):$${LD_LIBRARY_PATH}" ;\
		fi ; \
	fi ; \
	for package in $(TEST_PACKAGES); do \
		if [ -d $(CVSCOSRC)/$$package ] ; then \
			$(MAKE) -C $(CERN_BUILDDIR)/$$package MAKEOVERRIDES= \
				test || touch stampdir/cernlib-test-fail ; \
		fi ; \
	done ; \
	if [ -e stampdir/cernlib-test-fail ] ; then \
		echo "*** Failed test suite!!! ***" ; \
		exit 1 ; \
	else touch $@ ; fi
else
	touch $@
endif

clean:
	rm -rf stampdir src doc bin lib shlib build 200?
	rm -f $(PATCHDIR)/9[0-9][0-9]*.dpatch

.PHONY: all gunzip patch buildtree clean \
	cernlib-arch cernlib-arch-local cernlib-libraries cernlib-programs \
	cernlib-test cernlib-indep cernlib-indep-local

