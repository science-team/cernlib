Source: cernlib
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Lifeng Sun <lifongsun@gmail.com>
Standards-Version: 3.9.4
Priority: optional
Section: science
Homepage: http://cernlib.web.cern.ch/cernlib/
Build-Depends: po-debconf, dpkg-dev (>= 1.13.19), patch, mawk | gawk, xutils-dev, debhelper (>= 5.0.0), gfortran (>= 4:4.3), cfortran (>= 4.4-10), x11proto-core-dev, libxt-dev, libx11-dev, libmotif-dev, libblas-dev, liblapack-dev
Vcs-Git: git://anonscm.debian.org/debian-science/packages/cernlib.git
Vcs-Browser: http://anonscm.debian.org/gitweb/?p=debian-science/packages/cernlib.git

Package: cernlib-base-dev
Architecture: all
Section: devel
Depends: dpkg-dev, cernlib-base (= ${source:Version}), ${misc:Depends}
Replaces: cernlib-base (<< 2005.dfsg-6), libcojets1-dev (<= 2004.11.04.dfsg-0sarge1), libgeant1-dev (<= 2004.11.04.dfsg-0sarge1), libherwig59-dev (<= 2004.11.04.dfsg-0sarge1), libisajet758-dev (<= 2004.11.04.dfsg-0sarge1), libkuipx11-1-dev (<= 2004.11.04.dfsg-0sarge1), libmathlib1-dev (<= 2004.11.04.dfsg-0sarge1), libpaw1-dev (<= 2004.11.04.dfsg-0sarge1), libpdflib804-dev (<= 2004.11.04.dfsg-0sarge1), libphtools1-dev (<= 2004.11.04.dfsg-0sarge1)
Breaks: libcojets1-dev (<= 2004.11.04.dfsg-0sarge1), libgeant1-dev (<= 2004.11.04.dfsg-0sarge1), libherwig59-dev (<= 2004.11.04.dfsg-0sarge1), libisajet758-dev (<= 2004.11.04.dfsg-0sarge1), libkuipx11-1-dev (<= 2004.11.04.dfsg-0sarge1), libmathlib1-dev (<= 2004.11.04.dfsg-0sarge1), libpaw1-dev (<= 2004.11.04.dfsg-0sarge1), libpdflib804-dev (<= 2004.11.04.dfsg-0sarge1), libphtools1-dev (<= 2004.11.04.dfsg-0sarge1)
Conflicts: blas2-dev, lapack2-dev
Description: CERNLIB data analysis suite - dependencies checking script
 CERNLIB is a suite of data analysis tools and libraries created for
 use in physics experiments, but also with applications to other
 fields such as the biological sciences.
 .
 This package includes the "cernlib" script that lists the command-line options
 needed for linking against CERNLIB libraries. The script has been rewritten
 from the upstream version to calculate dependencies recursively. Also
 included are a contributed set of Autoconf macros to test for CERNLIB
 libraries, and a set of Imake macros to allow CERNLIB modules to be built
 out-of-tree.

Package: cernlib-base
Architecture: all
Section: devel
Multi-Arch: foreign
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends}
Suggests: vim-addon-manager
Description: CERNLIB data analysis suite - common files
 CERNLIB is a suite of data analysis tools and libraries created for
 use in physics experiments, but also with applications to other
 fields such as the biological sciences.
 .
 This package includes miscellaneous architecture-independent files useful
 for CERNLIB libraries and programs, including an example script that can
 generate a skeleton CERNLIB directory structure and Vim syntax highlighting
 macros for KUIPC CDF files and PAW "kumac" macro files.

Package: cernlib-core-dev
Architecture: all
Section: devel
Depends: cernlib-base (= ${source:Version}), cernlib-base-dev, libgraflib1-dev, libgrafx11-1-dev, libkernlib1-dev, libpacklib-lesstif1-dev, libmathlib2-dev, libpacklib1-dev, libpawlib2-dev, libpawlib-lesstif3-dev, kuipc, dzedit, nypatchy, ${misc:Depends}
Description: CERNLIB data analysis suite - core development files
 CERNLIB is a suite of data analysis tools and libraries created for
 use in physics experiments, but also with applications to other
 fields such as the biological sciences.
 .
 This metapackage provides the header files and static libraries needed
 by developers using the CERN libraries and not specifically interested in high
 energy physics. It also provides the CERNLIB development tools: DZedit,
 KUIPC, and the Nypatchy family of programs. CERNLIB analysis programs may be
 obtained by installing the cernlib-core metapackage.

Package: cernlib-core
Architecture: all
Depends: cernlib-base (= ${source:Version}), kxterm, paw++, paw, paw-common, paw-demos, ${misc:Depends}
Description: CERNLIB data analysis suite - main libraries and programs
 CERNLIB is a suite of data analysis tools and libraries created for
 use in physics experiments, but also with applications to other
 fields such as the biological sciences.
 .
 This metapackage provides the libraries and analysis tools (e.g. PAW) likely
 to be wanted by most users of the CERN libraries who are not interested
 specifically in high energy physics. It does not provide development
 libraries or tools; those may be obtained by installing the cernlib-core-dev
 metapackage or individual lib*-dev packages.

Package: cernlib-extras
Architecture: all
Depends: cernlib-base (= ${source:Version}), pawserv, zftp, ${misc:Depends}
Description: CERNLIB data analysis suite - extra programs
 CERNLIB is a suite of data analysis tools and libraries created for
 use in physics experiments, but also with applications to other
 fields such as the biological sciences.
 .
 This metapackage provides a few additional CERNLIB programs not included
 in any other CERNLIB package. Very few people are likely to be
 interested in them; currently they include zftp, pawserv and zserv.
 The latter two programs run as daemons through inetd and may
 raise concerns about the system's security.
 .
 Installing this package along with the 'cernlib' metapackage will supply
 a complete set of all CERNLIB programs and libraries, except for those
 not included in Debian due to licensing reasons.

Package: cernlib
Architecture: all
Depends: cernlib-base (= ${source:Version}), cernlib-core, cernlib-core-dev, cernlib-montecarlo, geant321-doc, geant321, ${misc:Depends}
Description: CERNLIB data analysis suite - general use metapackage
 CERNLIB is a suite of data analysis tools and libraries created for
 use in physics experiments, but also with applications to other
 fields such as the biological sciences.
 .
 This metapackage provides almost all of the programs and libraries contained
 in CERNLIB. Most people will likely want only a subset of these. A few
 extra CERNLIB programs, not of interest to many people, may be obtained via
 the cernlib-extras metapackage.

Package: dzedit
Architecture: any
Section: devel
Depends: ${shlibs:Depends}, ${misc:Depends}
Recommends: cernlib-base-dev, libpacklib1-dev
Description: CERNLIB data analysis suite - ZEBRA documentation editor
 CERNLIB is a suite of data analysis tools and libraries created for
 use in physics experiments, but also with applications to other
 fields such as the biological sciences.
 .
 DZedit is an interactive interface to the DZDOC (ZEBRA documentation) system
 developed at CERN. ZEBRA, part of the Packlib library, allows the creation
 of complex data structures in the FORTRAN 77 language; the DZDOC system
 helps to generate and maintain documentation for ZEBRA data structures.

Package: kuipc
Architecture: any
Section: devel
Depends: ${shlibs:Depends}, cernlib-base, ${misc:Depends}
Recommends: cernlib-base-dev, libpacklib1-dev
Suggests: libpacklib-lesstif1-dev
Description: CERNLIB data analysis suite - KUIP compiler
 CERNLIB is a suite of data analysis tools and libraries created for
 use in physics experiments, but also with applications to other
 fields such as the biological sciences.
 .
 KUIPC, the Kit for a User Interface Package Compiler, is a tool to simplify
 the writing of a program's user interface code. It takes
 a Command Definition File (CDF) as input, which describes the
 commands to be understood by the program, and outputs C or FORTRAN code that
 makes the appropriate function calls to set up the user interface. This
 code can then be compiled and linked with the rest of the program. Since
 the generated code uses KUIP routines, the program must also be linked
 against the Packlib library that contains them.
 .
 KUIPC is no longer actively developed, so aside from its use in the build
 process of CERNLIB, it is of mainly historical interest.

Package: kxterm
Architecture: any
Depends: ${shlibs:Depends}, cernlib-base, ${misc:Depends}
Description: CERNLIB data analysis suite - KUIP terminal emulator
 CERNLIB is a suite of data analysis tools and libraries created for
 use in physics experiments, but also with applications to other
 fields such as the biological sciences.
 .
 KXterm is a terminal emulator which combines the best features from
 the (now defunct) Apollo DM pads (like: input and transcript
 pads, automatic file backup of transcript pad, string search in pads,
 etc.) and the Korn shell emacs-style command line editing and command
 line recall mechanism. It can support the command-line interface
 of various CERNLIB applications.

Package: libgraflib1-dev
Architecture: any
Section: libdevel
Depends: libgraflib1-gfortran (= ${binary:Version}), libgrafx11-1-dev (= ${binary:Version}), ${misc:Depends}
Description: CERNLIB data analysis suite - graphical library (development files)
 CERNLIB is a suite of data analysis tools and libraries created for
 use in physics experiments, but also with applications to other
 fields such as the biological sciences.
 .
 The graflib library includes the HPLOT and DZDOC systems. HPLOT is a
 graphing facility intended to produce drawings and slides of histograms
 (and other data) of a quality suitable to talks and publications. DZDOC
 provides a method of viewing CERNLIB's ZEBRA data structures.
 .
 This package includes a static version of graflib, as well as C and FORTRAN
 header files.

Package: libgraflib1-gfortran
Architecture: any
Section: libs
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: CERNLIB data analysis suite - graphical library
 CERNLIB is a suite of data analysis tools and libraries created for
 use in physics experiments, but also with applications to other
 fields such as the biological sciences.
 .
 The graflib library includes the HPLOT and DZDOC systems. HPLOT is a
 graphing facility intended to produce drawings and slides of histograms
 (and other data) of a quality suitable to talks and publications. DZDOC
 provides a method of viewing CERNLIB's ZEBRA data structures.
 .
 In order to compile and link programs against this library,
 you must also install the libgraflib1-dev package.

Package: libgrafx11-1-dev
Architecture: any
Section: libdevel
Depends: libgrafx11-1-gfortran (= ${binary:Version}), libpacklib1-dev (= ${binary:Version}), libx11-dev (>= 4.3.0-3), ${misc:Depends}
Description: CERNLIB data analysis suite - interface to X11 and PostScript (development)
 CERNLIB is a suite of data analysis tools and libraries created for
 use in physics experiments, but also with applications to other
 fields such as the biological sciences.
 .
 This package includes a static version of libgrafX11, also known as HIGZ
 (High-level Interface to Graphics and ZEBRA), an interface to the X Window
 System. In addition to basic drawing functions, HIGZ includes a PostScript
 interface. Also included are C and FORTRAN header files.

Package: libgrafx11-1-gfortran
Architecture: any
Section: libs
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: CERNLIB data analysis suite - interface to X11 and PostScript
 CERNLIB is a suite of data analysis tools and libraries created for
 use in physics experiments, but also with applications to other
 fields such as the biological sciences.
 .
 This package includes libgrafX11, also known as HIGZ (High-level Interface
 to Graphics and ZEBRA), an interface to the X Window System. In addition
 to basic drawing functions, HIGZ includes a PostScript interface.
 .
 Note that in order to compile and link programs against this library,
 you must also install the libgrafx11-1-dev package.

Package: libkernlib1-dev
Architecture: any
Section: libdevel
Depends: libkernlib1-gfortran (= ${binary:Version}), cernlib-base-dev (>= 2006.dfsg.2-6), cfortran (>= 4.4-10), libc6-dev | libc-dev, ${misc:Depends}
Recommends: gfortran (>= 4:4.3)
Description: CERNLIB data analysis suite - core library of basic functions (development)
 CERNLIB is a suite of data analysis tools and libraries created for
 use in physics experiments, but also with applications to other
 fields such as the biological sciences.
 .
 The kernlib library contains a rather miscellaneous set of basic numerical,
 bitwise, and system-dependent functions used by other CERN libraries and
 programs.
 .
 This package includes the static version of kernlib, as well as
 C and FORTRAN header files.

Package: libkernlib1-gfortran
Architecture: any
Section: libs
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, cernlib-base, ${misc:Depends}
Description: CERNLIB data analysis suite - core library of basic functions
 CERNLIB is a suite of data analysis tools and libraries created for
 use in physics experiments, but also with applications to other
 fields such as the biological sciences.
 .
 The kernlib library contains a rather miscellaneous set of basic numerical,
 bitwise, and system-dependent functions used by other CERN libraries and
 programs.
 .
 Note that in order to compile and link programs against this library, you
 must also install the libkernlib1-dev package.

Package: libmathlib2-dev
Architecture: any
Section: libdevel
Depends: libblas-dev | libatlas-base-dev | libblas-3gf.so, liblapack-dev | libatlas-base-dev | liblapack-3gf.so, libmathlib2-gfortran (= ${binary:Version}), libkernlib1-dev (= ${binary:Version}), libpacklib1-dev (= ${binary:Version}), cfortran (>= 4.4-10), libc6-dev | libc-dev, ${misc:Depends}
Breaks: libmathlib1-dev (<= 2004.11.04.dfsg-0sarge1)
Conflicts: blas2-dev, lapack2-dev, refblas3-dev, lapack3-dev
Replaces: libmathlib1-dev (<= 2004.11.04.dfsg-0sarge1)
Description: CERNLIB data analysis suite - core mathematical library (development files)
 CERNLIB is a suite of data analysis tools and libraries created for
 use in physics experiments, but also with applications to other
 fields such as the biological sciences.
 .
 The mathlib library is a large set of mathematical routines used for purposes
 such as evaluating special functions and integrals, numerically solving
 algebraic and differential equations, performing matrix and vector operations,
 running statistical tests, and generating random numbers in specified
 distributions.
 .
 This package contains a static version of mathlib, as well
 as C and FORTRAN header files.

Package: libmathlib2-gfortran
Architecture: any
Section: libs
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: CERNLIB data analysis suite - core mathematical library
 CERNLIB is a suite of data analysis tools and libraries created for
 use in physics experiments, but also with applications to other
 fields such as the biological sciences.
 .
 The mathlib library is a large set of mathematical routines used for purposes
 such as evaluating special functions and integrals, numerically solving
 algebraic and differential equations, performing matrix and vector operations,
 running statistical tests, and generating random numbers in specified
 distributions.
 .
 In order to compile and link programs against this library, you
 must also install the libmathlib2-dev package.

Package: libpacklib-lesstif1-dev
Architecture: any
Section: libdevel
Depends: libpacklib-lesstif1-gfortran (= ${binary:Version}), libgrafx11-1-dev (= ${binary:Version}), libmotif-dev, libxt-dev (>= 4.3.0-3), ${misc:Depends}
Breaks: libkuipx11-1-dev (<= 2004.11.04.dfsg-0sarge1), libpacklib1-lesstif-dev (<< 2006.dfsg.2-6)
Replaces: libkuipx11-1-dev (<= 2004.11.04.dfsg-0sarge1), libpacklib1-lesstif-dev (<< 2006.dfsg.2-6)
Description: CERNLIB data analysis suite - core GUI library (development files)
 CERNLIB is a suite of data analysis tools and libraries created for
 use in physics experiments, but also with applications to other
 fields such as the biological sciences.
 .
 The packlib library includes a great deal of CERNLIB core functionality.
 Best-known are probably the MINUIT function minimization routines, the
 HBOOK histogramming routines, and KUIP (Kit for a User Interface Package).
 Other subsystems include CSPACK, a set of client-server routines; FFREAD,
 a format-free input processing system for FORTRAN; and ZEBRA, a data
 structure management package.
 .
 This package includes the graphical functionality of KUIP, the CERNLIB
 Kit for a User Interface Package. This library has been split off
 from packlib so that the packlib packages need not depend upon
 the X and Lesstif libraries.
 .
 This package was formerly named libkuipx11-1-dev. The text-based
 functionality of KUIP can be found in the libpacklib1-dev package.
 .
 This package provides the static version of the libpacklib-lesstif library,
 to be used for development.

Package: libpacklib-lesstif1-gfortran
Architecture: any
Section: libs
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}
Recommends: kxterm
Description: CERNLIB data analysis suite - core GUI library
 CERNLIB is a suite of data analysis tools and libraries created for
 use in physics experiments, but also with applications to other
 fields such as the biological sciences.
 .
 The packlib library includes a great deal of CERNLIB core functionality.
 Best-known are probably the MINUIT function minimization routines, the
 HBOOK histogramming routines, and KUIP (Kit for a User Interface Package).
 Other subsystems include CSPACK, a set of client-server routines; FFREAD,
 a format-free input processing system for FORTRAN; and ZEBRA, a data
 structure management package.
 .
 This package includes the graphical functionality of KUIP, the CERNLIB
 Kit for a User Interface Package. This library has been split off
 from packlib so that the packlib packages need not depend upon
 the X and Lesstif libraries.
 .
 This package was formerly named libkuipx11-1. The text-based functionality
 of KUIP can be found in the libpacklib1-gfortran package. 
 .
 Note that in order to compile and link programs against this library, you
 must also install the libpacklib-lesstif1-dev package.

Package: libpacklib1-dev
Architecture: any
Section: libdevel
Depends: libpacklib1-gfortran (= ${binary:Version}), libkernlib1-dev (= ${binary:Version}), cfortran (>= 4.4-10), libc6-dev | libc-dev, ${misc:Depends}
Suggests: libpacklib-lesstif1-dev
Description: CERNLIB data analysis suite - core library (development files)
 CERNLIB is a suite of data analysis tools and libraries created for
 use in physics experiments, but also with applications to other
 fields such as the biological sciences.
 .
 The packlib library includes a great deal of CERNLIB core functionality.
 Best-known are probably the MINUIT function minimization routines, the
 HBOOK histogramming routines, and KUIP (Kit for a User Interface Package).
 Other subsystems include CSPACK, a set of client-server routines; FFREAD,
 a format-free input processing system for FORTRAN; and ZEBRA, a data
 structure management package.
 .
 This package contains a static version of packlib. Also included are C
 and FORTRAN header files. Please note that the graphical functionality of
 KUIP has been split out into the separate libpacklib-lesstif1-dev package
 so that this package need not depend upon the X and Lesstif libraries.

Package: libpacklib1-gfortran
Architecture: any
Section: libs
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}
Suggests: xterm | x-terminal-emulator
Replaces: paw-static (<= 2005.05.09-1), paw++-static (<= 2005.05.09-1)
Breaks: paw-static (<= 2005.05.09-1), paw++-static (<= 2005.05.09-1)
Description: CERNLIB data analysis suite - core library
 CERNLIB is a suite of data analysis tools and libraries created for
 use in physics experiments, but also with applications to other
 fields such as the biological sciences.
 .
 The packlib library includes a great deal of CERNLIB core functionality.
 Best-known are probably the MINUIT function minimization routines, the
 HBOOK histogramming routines, and KUIP (Kit for a User Interface Package).
 Other subsystems include CSPACK, a set of client-server routines; FFREAD,
 a format-free input processing system for FORTRAN; and ZEBRA, a data
 structure management package.
 .
 This package also includes kuesvr, the KUIP edit server program that permits
 calling an editor asynchronously from within a KUIP-based program.
 .
 Please note that the graphical functionality of KUIP has been split out into
 the separate libpacklib-lesstif1-gfortran package so that this package need
 not depend upon the X libraries. In order to compile and link programs
 against libpacklib, you must also install the libpacklib1-dev (and
 maybe also the libpacklib-lesstif1-dev) package(s).

Package: nypatchy
Architecture: any
Section: devel
Depends: ${shlibs:Depends}, cernlib-base (>= 2006.dfsg.2-12), ${misc:Depends}
Recommends: gfortran | fortran-compiler, gcc | c-compiler
Description: CERNLIB data analysis suite - patch pre-processor for source code
 CERNLIB is a suite of data analysis tools and libraries created for
 use in physics experiments, but also with applications to other
 fields such as the biological sciences.
 .
 This package contains the Nypatchy family of programs, the successors to
 Patchy and Ypatchy.  These programs are intended for working with sets of
 patches (for instance, for use on different machine architectures) meant to
 be applied to a source code tree.  The patch sets and common source code are
 maintained as a single text-based Patchy Master file (PAM file).  PAM files
 can contain C, FORTRAN, or assembly code.
 .
 Nypatchy and related utilities can perform actions on a PAM file such as
 updating it; comparing it to an older version; or outputting source code
 suitable for input to a compiler, having selected some subset of available
 patches to use.  These utilities can be used interactively, or can run in
 batch mode from a "cradle" file of commands.
 .
 While Nypatchy is still used in places, mainly high-energy physics, these
 programs are no longer under active development.  They are not recommended
 for use in new projects.

Package: pawserv
Architecture: any
Section: net
Depends: ${shlibs:Depends}, ${misc:Depends}, netbase (>= 4.08), openbsd-inetd | inet-superserver
Recommends: cron
Suggests: logrotate
Conflicts: harden-servers
Breaks: zserv (<< 2003.08.21-1)
Replaces: zserv (<< 2003.08.21-1)
Description: CERNLIB data analysis suite - distributed PAW and file transfer servers
 CERNLIB is a suite of data analysis tools and libraries created for
 use in physics experiments, but also with applications to other
 fields such as the biological sciences.
 .
 This package contains the server daemon for remote PAW clients. PAW
 is the Physics Analysis Workstation, a graphical analysis program.
 Included in the same binary is zserv, the server daemon for the ZFTP protocol;
 zserv is a macro-extensible file transfer program which supports the
 transfer of formatted, unformatted and ZEBRA RZ files (CMZ, HBOOK, etc.).
 It is similar to a standard FTP daemon, although it listens on a different
 port. Both servers operate through inetd.
 .
 Since these servers accept passwords in clear text and do not support SSL
 or other encryption methods, you should probably only install this package
 within a trusted LAN. It should be seen as equivalent to vanilla telnetd
 in terms of security (or lack thereof).

Package: zftp
Architecture: any
Section: net
Depends: ${shlibs:Depends}, netbase (>= 4.08), ${misc:Depends}
Description: CERNLIB data analysis suite - file transfer program
 CERNLIB is a suite of data analysis tools and libraries created for
 use in physics experiments, but also with applications to other
 fields such as the biological sciences.
 .
 ZFTP is a macro-extensible file transfer program which supports the
 transfer of formatted, unformatted and ZEBRA RZ files (CMZ, HBOOK, etc.).
 It provides a common interface on all systems and avoids the problems of
 file format conversion that occur when transferring CERNLIB files
 between different architectures.
 .
 Except for the special treatment of CERNLIB files and use of a different port,
 zftp is very similar to an FTP client. Unless you are already familiar with
 this program, it will almost certainly be useless to you.

