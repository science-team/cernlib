You can choose which of these servers to run via Debconf.  If you do not
see a Debconf dialog at installation time, or if you want to change your
original choice, run "dpkg-reconfigure -plow pawserv".  The default is to
run only pawserv.  Of course you can also make this choice by editing
the file /etc/inetd.conf manually.

Notes on pawserv
----------------

This program is the server for distributed PAW, which runs via inetd.
Basically it allows a PAW client running on a remote host to access local
files.  The server must run as root in order to permit user logins (as it must
be able to read the /etc/shadow file).  Since this code has not, to my
knowledge, been audited for security, and because the remote PAW protocol
transmits passwords in cleartext, it would be wise to run pawserv only within a
LAN with trusted users that doesn't permit access from the Internet at large.

Notes on zserv
--------------

This is the server program for the ZEBRA FTP (zftp) protocol of CERNLIB, which
runs via inetd.  Except for listening on a different port and special treatment
of some files created by CERNLIB programs, it is similar to an FTP daemon.
The same security-related comments apply as above (the two programs actually
run from the same binary).


These programs by default write one log file per minute into /tmp.
In Debian, this behavior has been modified so that they write into a
/var/log/pawserv directory with permissions 0700.  A cron script
/etc/cron.hourly/pawserv is provided to concatenate these per-minute log
files into /var/log/pawserv.log every hour (the names are still
/var/log/pawserv and pawserv.log even if you only run zserv).

-- Kevin McCarty <kmccarty@debian.org>, Sun, 06 March 2005

