#!/bin/bash

# Cron script to concatenate pawserv / zserv's log files (it produces
# one per minute) into /var/log/pawserv.log

# Author: Kevin McCarty, 2005-03-06
# License: GPL version 2 or later (see /usr/share/common-licenses/GPL)

PATH=/bin:/usr/bin:/sbin:/usr/sbin
BINARY=/usr/sbin/zserv
LOGDIR=/var/log/pawserv
LOGFILE="${LOGDIR}.log"

[ -d "$LOGDIR" ] || exit 0

cd "$LOGDIR"
LOGS="$(find . -maxdepth 1 -type f -a -name zs\*.log -a -mmin +10 | sort)"
[ -n "$LOGS" ] || exit 0

set -e
umask 077

for log in $LOGS ; do
	if [ -s "$log" ] ; then
		YR="$( echo "$log" | cut -b 5-6)"
		MON="$(echo "$log" | cut -b 7-8)"
		DAY="$(echo "$log" | cut -b 9-10)"
		HR="$( echo "$log" | cut -b 11-12)"
		MIN="$(echo "$log" | cut -b 13-14)"
		echo "20${YR}-${MON}-${DAY} at ${HR}:${MIN}" >> "$LOGFILE"
		cat "$log" >> "$LOGFILE"
	fi
	
	rm -f "$log"
done

# If the package has been uninstalled and there are no more pending logs,
# try removing the log directory.

if [ ! -x "$BINARY" ] ; then
	rmdir "$LOGDIR" 2> /dev/null || true
fi

exit 0
