# Native Portuguese translation of cernlib.
# Copyright (C) 2006 THE cernlib'S COPYRIGHT HOLDER
# This file is distributed under the same license as the cernlib package.
# Hugo Peixoto <hugo.peixoto@fe.up.pt>, 2006.
# Miguel Figueiredo <elmig@debianpt.org>, 2008.
#
#
msgid ""
msgstr ""
"Project-Id-Version: cernlib 2006.dfsg.2-3\n"
"Report-Msgid-Bugs-To: Source: cernlib@packages.debian.org\n"
"POT-Creation-Date: 2007-12-12 00:38+0530\n"
"PO-Revision-Date: 2008-01-01 20:58+0000\n"
"Last-Translator: Miguel Figueiredo <elmig@debianpt.org>\n"
"Language-Team: Portuguese <traduz@debianpt.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: select
#. Choices
#. Translators: "Both" means both "Pawserv" and "Zserv" to the question
#. about servers to be run
#: ../pawserv.templates:2001
msgid "Both"
msgstr "Ambos"

#. Type: select
#. Description
#: ../pawserv.templates:2002
msgid "Servers to be run from inetd:"
msgstr "Servidores a correrem a partir do inetd:"

#. Type: select
#. Description
#: ../pawserv.templates:2002
msgid ""
"This package includes both the pawserv daemon (permitting remote hosts to "
"read local files while running PAW/Paw++) and the zserv daemon (allowing "
"remote hosts to log in using CERN's ZFTP protocol)."
msgstr ""
"Este pacote inclui tanto o daemon pawserv (permitindo que anfitriões remotos "
"leiam ficheiros locais enquanto correm PAW/Pawpp) como o daemon zserv "
"(permitindo que anfitriões remotos utilizem o protocolo ZFTP da CERN para se "
"autenticarem)."

#. Type: select
#. Description
#: ../pawserv.templates:2002
msgid ""
"These servers are run from the inetd superserver and either both or only one "
"of them may be enabled. Enabling 'pawserv' alone is sufficient for most "
"users."
msgstr ""
"Estes servidores correm a partir do superservidor inetd e pode ser activado "
"um deles ou ambos. Permitir apenas o 'pawserv' é suficiente para a maioria "
"dos utilizadores."

#~ msgid "Pawserv, Zserv, Both"
#~ msgstr "Pawserv, Zserv, Ambos"
