#!/usr/bin/make -f

MY_NAME = cernlib
MY_MODULES = Imakefile car cfortran config graflib include \
	     man mathlib p5boot packlib patchy scripts 

export DEB_BUILD_OPTIONS=nocheck

include debian/cernlib-debian.mk

binary-arch: install-arch
	dh_testdir
	dh_testroot
	dh_install -s
	# move a couple files around
	PL=debian/libpacklib1-gfortran/usr ; \
	mkdir -p $$PL/lib/$(DEB_HOST_MULTIARCH)/libpacklib1-gfortran/; \
	mv -f $$PL/bin/kuesvr \
		$$PL/lib/$(DEB_HOST_MULTIARCH)/libpacklib1-gfortran/ ; \
	mv -f $$PL/share/man/man1/kuesvr.1 $$PL/share/man/man7/kuesvr.7 ; \
	sed -i -e '/^\.TH/s/1/7/' $$PL/share/man/man7/kuesvr.7 ; \
	rmdir $$PL/bin $$PL/share/man/man1
	dh_installdocs -s
	dh_installexamples -s
	dh_installmenu -s
	dh_installman -s
	dh_installchangelogs -s
	dh_installdebconf -s
	dh_strip -s
	dh_link -s
	dh_lintian -s
	dh_compress -s
	dh_installcron -s
	dh_installlogrotate -s
	dh_fixperms -s
	for package in libgraflib1 libgrafx11-1 libkernlib1 \
		libmathlib2 libpacklib-lesstif1 libpacklib1 ; do \
		dh_makeshlibs -p$${package}-gfortran ; \
	done
	dh_shlibdeps -s -lshlib
	dh_installdeb -s
	dh_gencontrol -s
	dh_md5sums -s
	dh_builddeb -s

binary-indep: install-indep
	dh_testdir
	dh_testroot
	dh_install -i
	dh_installdocs -i
	dh_installexamples -i
	dh_installman -i
	dh_installchangelogs -i
	dh_link -i
	dh_lintian -i
	dh_compress -i -X.dat -X.kumac -X.F -X.sh

	# Fudge metapackage docs a little bit for efficiency:
	for package in cernlib cernlib-core cernlib-core-dev cernlib-extras \
		cernlib-base-dev ; do \
		rm -rf debian/$$package/usr/share/doc/$$package ; \
		ln -sf cernlib-base debian/$$package/usr/share/doc/$$package ; \
	done

	dh_fixperms -i
	chmod a+x debian/cernlib-base/usr/share/doc/cernlib-base/examples/*.sh
	chmod a+x debian/cernlib-base-dev/usr/share/cernlib/remove-deadpool
	dh_installdeb -i
	dh_gencontrol -i
	dh_md5sums -i
	dh_builddeb -i

binary: binary-indep binary-arch

clean: update-po
update-po:
	debconf-updatepo

.PHONY: install-arch-local binary binary-indep binary-arch clean update-po

